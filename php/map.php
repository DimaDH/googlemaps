<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
    #map{
        width: 100%;
        height: 550px;
        background-color: grey;
    }
    #title{
        margin-left: 45%;
    }
</style>
<body>

<h3 id="title">My Google Maps</h3>
<div id="map"></div>

<script>
    var neighborhoods = {lat: 0, lng: 0};
    var markers = [];
    var map;

    var download = setInterval(function() {
        $.ajax({
            type: "POST",
            url: "selectdb.php",
            dataType: "json",
            success: function(data)
            {
                if(neighborhoods.lat != +data.latitude && neighborhoods.lng != +data.longitude){
                    neighborhoods.lat = +data.latitude;
                    neighborhoods.lng = +data.longitude;
                    drop();
                    console.log(data)
                }
            }
        });
    }, 10000);
    
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 2,
            center: {lat: 0, lng: 0}
        });
    }

    function drop() {
        clearMarkers();
            addMarkerWithTimeout(neighborhoods);
    }

    function addMarkerWithTimeout(position) {
            markers.push(new google.maps.Marker({
                position: position,
                map: map,
                animation: google.maps.Animation.DROP
            }));
    }

    function clearMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }



</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEDWncOBC_Daj-zk1k2Z-Acp6TS3oRbew&callback=initMap">
</script>

</body>
</html>
<?php

require_once 'connection.php';

$link = mysqli_connect($host, $user, $password, $database)
or die("Ошибка " . mysqli_error($link));

$select = "SELECT * FROM coordinates order by `id_coordinates` DESC limit 1";


$result_select = mysqli_query($link , $select);

while($row = mysqli_fetch_array($result_select)) {

    $arr_coordinates = array('latitude' => $row['latitude'], 'longitude' => $row['longitude']);

    echo json_encode($arr_coordinates);

    return ($arr_coordinates);

}
mysqli_close($link);